/*
   There is conflict with 'semi' and 'no-extra-semi' rules on interfaces.
   Interfaces MUST NOT have semi-collon after the last },
   while 'export default' MUST have semi-collon after the last }.
   The solution is to declare Interface, and then export it,
   never directly 'export default interface'
*/

module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ["plugin:vue/essential", "@vue/typescript/recommended"],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/camelcase": "off",
    "@typescript-eslint/interface-name-prefix": "off",
    semi: [2, "always"],
  },
};
