import firebase from 'firebase/app';

export default class FirebaseErrorUtils {
  static getTranslation(error: any) {
    const authError = error as firebase.auth.Error;

    if (authError) {
      return this.getAuthTranslation(authError);
    }

    return '';
  }

  protected static getAuthTranslation(error: firebase.auth.Error): string {
    switch (error.code) {
      case 'auth/email-already-exists':
        return 'errors.firebase.auth.emailAlreadyExists';
      case 'auth/email-already-in-use':
        return 'errors.firebase.auth.emailAlreadyInUse';
      case 'auth/invalid-email':
        return 'errors.firebase.auth.invalidEmail';
      case 'auth/invalid-password':
        return 'errors.firebase.auth.invalidPassword';
      case 'auth/wrong-password':
        return 'errors.firebase.auth.wrongPassword';
      case 'auth/user-not-found':
        return 'errors.firebase.auth.userNotFound';
      case 'auth/too-many-requests':
        return 'errors.firebase.auth.tooManyRequests';
      default:
        return 'errors.unknown';
    }
  }
}
