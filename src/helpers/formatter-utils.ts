import i18n from "@/plugins/i18n";
import store from "@/store";

export default class FormatterUtils {
  public static numberToPrice(number: number): string {
    const formatter = new Intl.NumberFormat(i18n.locale, {
      style: "currency",
      currency: store.getters.defaultCurrency,
    });

    return formatter.format(number);
  }

  public static numberToDigitNumber(number: string): string {
    const formattedNumber = String(number).replace(/(.)(?=(\d{3})+$)/g, "$1,");

    return formattedNumber;
  }
}
