import Vue from "vue";
import App from "@/App.vue";
import router from "@/router";
import store from "@/store";
import vuetify from "@/plugins/vuetify";
import i18n from "@/plugins/i18n";
import { services } from "@/data/di";
import "@/plugins/firebase";
import "@/plugins/vue-meta";
import "@/plugins/vue-clipboard2";
import "@/assets/index.css";
import ViewContainer from "@/components/atoms/ViewContainer.vue";

Vue.config.productionTip = false;

// Global components
Vue.component("ViewContainer", ViewContainer);

// Dependancy Injection
Vue.prototype.$services = services;

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
