import State from "./state";

export default {
  isApplicationReady(state: State) {
    return state.isApplicationReady;
  },
  isDrawerMini(state: State) {
    return state.isDrawerMini;
  },
  currentUser(state: State) {
    return state.currentUser;
  },
  currenciesTicker(state: State) {
    return state.currenciesTicker;
  },
  currencySparkline(state: State) {
    return state.currencySparkline;
  },
  users(state: State) {
    return state.users;
  },
  defaultCurrency(state: State) {
    return state.defaultCurrency;
  },
};
