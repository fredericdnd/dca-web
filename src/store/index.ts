import Vue from "vue";
import Vuex from "vuex";
import mutations from "./mutations";
import getters from "./getters";
import actions from "./actions";
import State from "./state";
import StorageService from "@/data/services/storage-service";

Vue.use(Vuex);

export default new Vuex.Store<State>({
  state: {
    isApplicationReady: false,
    isDrawerMini: false,
    currentUser: null,
    currenciesTicker: [],
    currencySparkline: null,
    users: [],
    defaultCurrency: StorageService.get("defaultCurrency") || "USD",
  },
  mutations: mutations,
  getters: getters,
  actions: actions,
});
