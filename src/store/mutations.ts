import State from "@/store/state";
import firebase from "firebase/app";
import User from "@/data/models/user";
import store from "@/store";
import Ticker from "@/data/models/ticker";
import StorageService from "@/data/services/storage-service";
import Sparkline from "@/data/models/sparkline";

export default {
  setCurrentUser(state: State, currentUser: firebase.User | null) {
    state.currentUser = currentUser;
  },
  setApplicationReady(state: State) {
    state.isApplicationReady = true;
  },
  setDrawerMini(state: State, mini: boolean) {
    state.isDrawerMini = mini;
  },
  setCurrenciesTicker(state: State, currenciesTicker: Ticker[]) {
    state.currenciesTicker = currenciesTicker;
  },
  setCurrencySparkline(state: State, currencySparkline: Sparkline) {
    state.currencySparkline = currencySparkline;
  },
  setUsers(state: State, users: User[]) {
    state.users = users;
  },
  setDefaultCurrency(state: State, defaultCurrency: string) {
    store.commit("setCurrenciesTicker", null);
    StorageService.set("defaultCurrency", defaultCurrency);
    state.defaultCurrency = defaultCurrency;
  },
};
