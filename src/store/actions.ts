import SparklineService from "@/data/services/sparkline-service";
import TickerService from "@/data/services/ticker-service";
import UsersService from "@/data/services/users-service";
import store from "@/store";

export default {
  toggleDrawer() {
    const newValue = !store.getters.isDrawerMini;
    store.commit("setDrawerMini", newValue);
  },

  async getCurrenciesTicker(): Promise<void> {
    const currenciesTicker = await TickerService.getCurrenciesTicker();
    store.commit("setCurrenciesTicker", currenciesTicker);
    return await Promise.resolve();
  },

  async getCurrencySparkline(
    _context: any,
    payload: { currency: string; start: Date; end: Date }
  ): Promise<void> {
    const currencySparkline = await SparklineService.getCurrencySparkline(
      payload.currency,
      payload.start,
      payload.end
    );

    store.commit("setCurrencySparkline", currencySparkline[0]);

    return await Promise.resolve();
  },

  async observeAllUsers() {
    UsersService.observeAllUsers(
      (users) => {
        store.commit("setUsers", users);
      },
      (error) => Promise.reject(error)
    );
  },
};
