import firebase from "firebase/app";
import User from "@/data/models/user";
import Ticker from "@/data/models/ticker";
import Sparkline from "@/data/models/sparkline";

interface State {
  isApplicationReady: boolean;
  isDrawerMini: boolean;
  currentUser: firebase.User | null;
  currenciesTicker: Ticker[];
  currencySparkline: Sparkline | null;
  users: User[];
  defaultCurrency: string;
}

export default State;
