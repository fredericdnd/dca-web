import StorageService from "@/data/services/storage-service";
import Vue from "vue";
import Vuetify, { VSnackbar, VBtn, VIcon } from "vuetify/lib";
import Notify from "@/plugins/notify";

Vue.use(Notify, { $vuetify: new Vuetify().framework });

Vue.use(Vuetify, {
  components: {
    VSnackbar,
    VBtn,
    VIcon,
  },
});

const darkMode = JSON.parse(
  StorageService.get("darkMode") || "false"
) as boolean;

export default new Vuetify({
  theme: {
    dark: darkMode,
    themes: {
      light: {
        primary: {
          base: "#4f46e5",
          darken1: "#3a34b3",
          darken2: "#322c99",
        },
        background: "#f2f3f8",
      },
      dark: {
        primary: {
          base: "#6961ed",
          darken1: "#5953c9",
          darken2: "#4c47ad",
        },
        background: "#121212",
      },
    },
  },
});
