import firebase from 'firebase/app';
import config from '@/config/index';

firebase.initializeApp(config.firebase);
