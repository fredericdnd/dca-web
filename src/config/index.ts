export default {
  firebase: {
    apiKey: "AIzaSyDbMXz2BOrPUWyUWH6hCsGLDIAZFprtM0M",
    authDomain: "dca-web-5cfb6.firebaseapp.com",
    projectId: "dca-web-5cfb6",
    storageBucket: "dca-web-5cfb6.appspot.com",
    messagingSenderId: "13803233175",
    appId: "1:13803233175:web:1b9c77671720d6ba67aed2",
    measurementId: "G-4ELB36M3JC",
  },
  nomicsApi: String(process.env.VUE_APP_NOMICS_BASE_URL),
};
