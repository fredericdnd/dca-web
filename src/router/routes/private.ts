import Home from "@/views/Home.vue";
import Users from "@/views/Users.vue";
import Market from "@/views/Market.vue";
import Settings from "@/views/Settings.vue";
import { RouteConfig } from "vue-router";

const privateRoutes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      isSecured: true,
      showDrawer: true,
      showHeader: true,
      title: "home.title",
    },
  },
  {
    path: "/users",
    name: "Users",
    component: Users,
    meta: {
      isSecured: true,
      showDrawer: true,
      showHeader: true,
      title: "users.title",
    },
  },
  {
    path: "/market",
    name: "Market",
    component: Market,
    meta: {
      isSecured: true,
      showDrawer: true,
      showHeader: true,
      title: "market.title",
    },
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings,
    meta: {
      isSecured: true,
      showDrawer: true,
      showHeader: true,
      title: "settings.title",
    },
  },
];

export default privateRoutes;
