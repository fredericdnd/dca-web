import TickerDao from "@/data/dao/ticker-dao";
import Ticker from "@/data/models/ticker";

export default class TickerService {
  private static tickerDao = new TickerDao();

  static getCurrenciesTicker(): Promise<Ticker[]> {
    return this.tickerDao.getCurrenciesTicker();
  }
}
