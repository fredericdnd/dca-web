export default class StorageService {
  static set(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  static get(key: string): string | null {
    return localStorage.getItem(key);
  }
}
