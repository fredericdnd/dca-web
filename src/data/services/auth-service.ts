import firebase from "firebase/app";
import "firebase/auth";
import AuthDao from "@/data/dao/auth-dao";

export default class AuthService {
  private static authDao = new AuthDao();

  public static get isSignedIn(): boolean {
    return this.authDao.isSignedIn;
  }

  static observeCurrentUser() {
    return this.authDao.observeCurrentUser();
  }

  static signIn(
    email: string,
    password: string
  ): Promise<firebase.auth.UserCredential> {
    return this.authDao.signIn(email, password);
  }

  static signUp(
    email: string,
    password: string
  ): Promise<firebase.auth.UserCredential> {
    return this.authDao.signUp(email, password);
  }

  static authWithGoogle(): Promise<firebase.auth.UserCredential> {
    return this.authDao.authWithGoogle();
  }

  static resetPassword(email: string): Promise<void> {
    return this.authDao.resetPassword(email);
  }

  static signOut(): Promise<void> {
    return this.authDao.signOut();
  }
}
