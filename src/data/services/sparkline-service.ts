import SparklineDao from "@/data/dao/sparkline-dao";
import Sparkline from "@/data/models/sparkline";

export default class SparklineService {
  private static sparklineDao = new SparklineDao();

  static getCurrencySparkline(
    currency: string,
    start: Date,
    end: Date
  ): Promise<Sparkline[]> {
    return this.sparklineDao.getCurrencySparkline(currency, start, end);
  }
}
