import UsersDao from "@/data/dao/users-dao";
import User from "@/data/models/user";
import firebase from "firebase/app";

export default class UsersService {
  private static usersDao = new UsersDao();

  public static usersObserver: (() => void) | null;

  static observeAllUsers(
    onNext: (array: User[]) => void,
    onError: (error: firebase.firestore.FirestoreError) => void
  ) {
    if (this.usersObserver) {
      this.usersObserver();
      this.usersObserver = null;
    }

    this.usersObserver = this.usersDao.observeAllUsers(
      (users) => onNext(users),
      (error) => onError(error)
    );
  }

  static getUserById(id: string): Promise<User | null> {
    return this.usersDao.getUserById(id);
  }

  static createUser(
    userCredentials: firebase.auth.UserCredential,
    newUser: User
  ): Promise<void> {
    if (!userCredentials.user) {
      throw new Error("Invalid credentials user");
    }

    return this.usersDao.create(userCredentials.user.uid, newUser);
  }

  static updateUser(user: User): Promise<void> {
    return this.usersDao.update(user.id, user);
  }
}
