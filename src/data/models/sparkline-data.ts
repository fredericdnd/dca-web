export class SparklineData {
  timestamp: Date;
  price: string;

  constructor(timestamp: Date, price: string) {
    this.timestamp = timestamp;
    this.price = price;
  }
}
