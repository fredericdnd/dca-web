import BaseModel from "@/data/models/base/base-model";
import TypeUtils from "@/helpers/type-utils";

export default class User extends BaseModel {
  email: string | null = null;
  firstName: string | null = null;
  lastName: string | null = null;

  static map(data: Partial<User> | null, id: string | null): User {
    const user: User = new User();

    user.id = TypeUtils.hasStringOrDefault(id, data?.id);
    user.firstName = TypeUtils.hasStringOrDefault(data?.firstName);
    user.email = TypeUtils.hasStringOrDefault(data?.email);
    user.lastName = TypeUtils.hasStringOrDefault(data?.lastName);

    return user;
  }

  toJSON() {
    return {
      id: this.id,
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
    };
  }

  copy(): User {
    return User.map(this.toJSON(), this.id);
  }
}
