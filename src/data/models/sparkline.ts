import TypeUtils from "@/helpers/type-utils";
import { SparklineData } from "@/data/models/sparkline-data";

export default class Sparkline {
  currency: string | null = null;
  timestamps: Date[] | null = null;
  prices: string[] | null = null;

  static map(data: Partial<Sparkline> | null): Sparkline {
    const newData: Sparkline = new Sparkline();

    newData.currency = TypeUtils.hasStringOrDefault(data?.currency);
    newData.timestamps = TypeUtils.hasArrayOrDefault(data?.timestamps).map(
      (t) => new Date(t)
    );
    newData.prices = TypeUtils.hasArrayOrDefault(data?.prices);

    return newData;
  }

  toData(): SparklineData[] {
    const data: SparklineData[] = [];

    const timestamps = this.timestamps || [];
    const prices = this.prices || [];

    timestamps.forEach((timestamp, index) => {
      const price = prices[index];

      const sparklineData = new SparklineData(timestamp, price);
      data.push(sparklineData);
    });

    return data;
  }
}
