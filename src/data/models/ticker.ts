import TypeUtils from "@/helpers/type-utils";
import BaseModel from "@/data/models/base/base-model";

export default class Ticker extends BaseModel {
  currency: string | null = null;
  status: string | null = null;
  price: string | null = null;
  price_date: Date | null = null;
  price_timestamp: Date | null = null;
  symbol: string | null = null;
  circulating_supply: string | null = null;
  max_supply: string | null = null;
  name: string | null = null;
  logo_url: string | null = null;
  market_cap: string | null = null;
  transparent_market_cap: string | null = null;
  num_exchanges: string | null = null;
  num_pairs: string | null = null;
  num_pairs_unmapped: string | null = null;
  first_candle: Date | null = null;
  first_trade: Date | null = null;
  first_order_book: Date | null = null;
  first_priced_at: Date | null = null;
  rank: string | null = null;
  rank_delta: string | null = null;
  high: string | null = null;
  high_timestamp: Date | null = null;

  static map(data: Partial<Ticker> | null): Ticker {
    const newData: Ticker = new Ticker();
    
    newData.id = TypeUtils.hasStringOrDefault(data?.id);
    newData.currency = TypeUtils.hasStringOrDefault(data?.currency);
    newData.status = TypeUtils.hasStringOrDefault(data?.status);
    newData.price = TypeUtils.hasStringOrDefault(data?.price);
    newData.price_date = TypeUtils.hasDateOrDefault(data?.price_date || "");
    newData.price_timestamp = TypeUtils.hasDateOrDefault(
      data?.price_timestamp || ""
    );
    newData.symbol = TypeUtils.hasStringOrDefault(data?.symbol);
    newData.circulating_supply = TypeUtils.hasStringOrDefault(
      data?.circulating_supply
    );
    newData.max_supply = TypeUtils.hasStringOrDefault(data?.max_supply);
    newData.name = TypeUtils.hasStringOrDefault(data?.name);
    newData.logo_url = TypeUtils.hasStringOrDefault(data?.logo_url);
    newData.market_cap = TypeUtils.hasStringOrDefault(data?.market_cap);
    newData.transparent_market_cap = TypeUtils.hasStringOrDefault(
      data?.transparent_market_cap
    );
    newData.num_exchanges = TypeUtils.hasStringOrDefault(data?.num_exchanges);
    newData.num_pairs = TypeUtils.hasStringOrDefault(data?.num_pairs);
    newData.num_pairs_unmapped = TypeUtils.hasStringOrDefault(
      data?.num_pairs_unmapped
    );
    newData.first_candle = TypeUtils.hasDateOrDefault(data?.first_candle || "");
    newData.first_trade = TypeUtils.hasDateOrDefault(data?.first_trade || "");
    newData.first_order_book = TypeUtils.hasDateOrDefault(
      data?.first_order_book || ""
    );
    newData.first_priced_at = TypeUtils.hasDateOrDefault(
      data?.first_priced_at || ""
    );
    newData.rank = TypeUtils.hasStringOrDefault(data?.rank);
    newData.rank_delta = TypeUtils.hasStringOrDefault(data?.rank_delta);
    newData.high = TypeUtils.hasStringOrDefault(data?.high);
    newData.high_timestamp = TypeUtils.hasDateOrDefault(
      data?.high_timestamp || ""
    );

    return newData;
  }
}
