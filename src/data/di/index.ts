import { Store } from "vuex";
import AuthService from "@/data/services/auth-service";
import UsersService from "@/data/services/users-service";
import TickerService from "@/data/services/ticker-service";
import StorageService from "@/data/services/storage-service";

export const services = {
  users: UsersService,
  auth: AuthService,
  ticker: TickerService,
  storage: StorageService,
};

declare module "vue/types/vue" {
  interface Vue {
    $services: {
      users: typeof UsersService;
      auth: typeof AuthService;
      ticker: typeof TickerService;
      storage: typeof StorageService;
    };
    $store: Store<any>;
  }
}
