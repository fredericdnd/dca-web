import User from "@/data/models/user";
import AbstractFirebaseFirestoreDao from "@/data/dao/abstract-firebase-firestore-dao";
import FirestoreReferences from "@/helpers/firestore-references";
import { IUsersDao } from "@/data/dao/interfaces/i-users-dao";
import firebase from "firebase/app";

export default class UsersDao extends AbstractFirebaseFirestoreDao<User>
  implements IUsersDao {
  constructor() {
    super(FirestoreReferences.USERS);
  }

  observeAllUsers(
    onNext: (array: User[]) => void,
    onError: (error: firebase.firestore.FirestoreError) => void
  ): () => void {
    return super.observeAll(
      (data) => {
        const users = data.map((item) => User.map(item, item.id || ""));
        onNext(users);
      },
      (error) => onError(error)
    );
  }

  getUserById(id: string): Promise<User | null> {
    return super.getById(id).then((data) => User.map(data, id));
  }

  updateUser(user: User): Promise<void> {
    return super.update(user.id, user);
  }
}
