import { IAuthDao } from "@/data/dao/interfaces/i-auth-dao";
import store from "@/store";
import firebase from "firebase/app";
import "firebase/auth";

export default class AuthDao implements IAuthDao {
  get isSignedIn(): boolean {
    return firebase.auth().currentUser !== null;
  }

  observeCurrentUser(): void {
    firebase.auth().onAuthStateChanged((user) => {
      store.commit("setCurrentUser", user);
      store.commit("setApplicationReady");
    });
  }

  signIn(
    email: string,
    password: string
  ): Promise<firebase.auth.UserCredential> {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  signUp(
    email: string,
    password: string
  ): Promise<firebase.auth.UserCredential> {
    return firebase.auth().createUserWithEmailAndPassword(email, password);
  }

  authWithGoogle(): Promise<firebase.auth.UserCredential> {
    const provider = new firebase.auth.GoogleAuthProvider();
    return firebase.auth().signInWithPopup(provider);
  }

  resetPassword(email: string): Promise<void> {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  signOut(): Promise<void> {
    return firebase.auth().signOut();
  }
}
