import AbstractApiDao from "@/data/dao/abstract-api-dao";
import { ITickerDao } from "@/data/dao/interfaces/i-ticker-dao";
import config from "@/config";
import store from "@/store";
import Ticker from "@/data/models/ticker";

export default class TickerDao extends AbstractApiDao<Ticker>
  implements ITickerDao {
  async getCurrenciesTicker(): Promise<Ticker[]> {
    const data = await super.getAll(config.nomicsApi + "/currencies/ticker", {
      params: {
        key: process.env.VUE_APP_NOMICS_API_KEY,
        convert: store.getters.defaultCurrency,
        "per-page": 100,
      },
    });

    if (!data) {
      return Promise.reject();
    }

    return data.map((d) => Ticker.map(d));
  }
}
