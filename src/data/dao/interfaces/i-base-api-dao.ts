import { AxiosRequestConfig } from "axios";

interface IBaseApiDao<T> {
  get(path: string, config?: AxiosRequestConfig): Promise<T | null>;

  getAll(path: string, config?: AxiosRequestConfig): Promise<T[]>;

  post(path: string, data: any, config?: AxiosRequestConfig): Promise<void>;

  patch(path: string, data: any, config?: AxiosRequestConfig): Promise<void>;

  delete(path: string, config?: AxiosRequestConfig): Promise<void>;
}

export default IBaseApiDao;
