import Sparkline from "@/data/models/sparkline";

export interface ISparklineDao {
    getCurrencySparkline(currency: string, start: Date, end: Date): Promise<Sparkline[]>;
}
