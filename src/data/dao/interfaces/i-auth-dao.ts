import firebase from "firebase/app";

export interface IAuthDao {
  readonly isSignedIn: boolean;

  observeCurrentUser(): void;

  signIn(
    email: string,
    password: string
  ): Promise<firebase.auth.UserCredential>;

  signUp(
    email: string,
    password: string
  ): Promise<firebase.auth.UserCredential>;

  authWithGoogle(): Promise<firebase.auth.UserCredential>;

  resetPassword(email: string): Promise<void>;

  signOut(): Promise<void>;
}
