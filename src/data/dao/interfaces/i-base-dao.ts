import firebase from "firebase/app";

interface IBaseDao<T> {
  getById(id: string): Promise<T | null>;

  observeAll(
    callback: (array: T[]) => void,
    onError: (error: firebase.firestore.FirestoreError) => void
  ): () => void;

  getAll(): Promise<T[]>;

  update(id: string | null, data: T): Promise<void>;
}

export default IBaseDao;
