import Ticker from "@/data/models/ticker";

export interface ITickerDao {
  getCurrenciesTicker(): Promise<Ticker[]>;
}
