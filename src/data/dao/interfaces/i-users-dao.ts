import User from "@/data/models/user";
import firebase from "firebase/app";

export interface IUsersDao {
  observeAllUsers(
    onNext: (array: User[]) => void,
    onError: (error: firebase.firestore.FirestoreError) => void
  ): () => void;

  getUserById(id: string): Promise<User | null>;
}
