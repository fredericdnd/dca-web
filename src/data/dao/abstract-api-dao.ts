import axios, { AxiosRequestConfig } from "axios";
import IBaseApiDao from "@/data/dao/interfaces/i-base-api-dao";

export default class AbstractApiDao<T> implements IBaseApiDao<T> {
  public get(path: string, config?: AxiosRequestConfig): Promise<T | null> {
    return axios.get(path, config).then((response) => response.data);
  }

  public getAll(path: string, config?: AxiosRequestConfig): Promise<T[]> {
    return axios.get(path, config).then((response) => response.data);
  }

  public post(
    path: string,
    data: any,
    config?: AxiosRequestConfig
  ): Promise<void> {
    return axios.post(path, data, config);
  }

  public patch(
    path: string,
    data: any,
    config?: AxiosRequestConfig
  ): Promise<void> {
    return axios.patch(path, data, config);
  }

  public delete(path: string, config?: AxiosRequestConfig): Promise<void> {
    return axios.delete(path, config);
  }
}
