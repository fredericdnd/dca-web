import IBaseDao from "@/data/dao/interfaces/i-base-dao";
import firebase from "firebase/app";
import "firebase/firestore";
import CollectionReference = firebase.firestore.CollectionReference;

export default class AbstractFirebaseFirestoreDao<T> implements IBaseDao<T> {
  constructor(protected reference: string) {}

  public getReferenceCollection(): CollectionReference {
    return firebase.firestore().collection(this.reference);
  }

  public getAll(): Promise<T[]> {
    return this.getReferenceCollection()
      .get()
      .then((snapshot) => AbstractFirebaseFirestoreDao.toArray(snapshot));
  }

  public observeAll(
    onNext: (array: T[]) => void,
    onError: (error: firebase.firestore.FirestoreError) => void
  ): () => void {
    return this.getReferenceCollection().onSnapshot(
      (snapshot) => onNext(AbstractFirebaseFirestoreDao.toArray(snapshot)),
      (error) => onError(error)
    );
  }

  public getById(id: string): Promise<T | null> {
    if (!id) {
      return Promise.resolve(null);
    }
    return this.getReferenceCollection()
      .doc(id)
      .get()
      .then((r) => AbstractFirebaseFirestoreDao.objectElseNull<T>(r));
  }

  public update(id: string | null, data: T): Promise<void> {
    if (!id) {
      throw new Error("Id is null");
    }

    return this.getReferenceCollection()
      .doc(id)
      .update(JSON.parse(JSON.stringify(data)));
  }

  public create(id: string | null, data: T): Promise<void> {
    if (!id) {
      throw new Error("Id is null");
    }

    return this.getReferenceCollection()
      .doc(id)
      .set(JSON.parse(JSON.stringify(data)));
  }

  protected static objectElseNull<T>(doc: any): T | null {
    if (doc === null || !doc.exists) {
      return null;
    }

    const val = doc.data();
    val.id = doc.id;

    if (!val) {
      return null;
    }

    return val;
  }

  protected static toArray(data: any): any[] {
    if (!data || !data.docs) {
      return [];
    }

    const docs: any[] = [];
    data.docs.forEach((doc: any) => {
      docs.push({ ...doc.data(), id: doc.id });
    });

    return docs;
  }
}
