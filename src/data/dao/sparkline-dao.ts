import AbstractApiDao from "@/data/dao/abstract-api-dao";
import { ISparklineDao } from "@/data/dao/interfaces/i-sparkline-dao";
import config from "@/config";
import store from "@/store";
import Sparkline from "@/data/models/sparkline";

export default class SparklineDao extends AbstractApiDao<Sparkline>
  implements ISparklineDao {
  async getCurrencySparkline(
    currency: string,
    start: Date,
    end: Date
  ): Promise<Sparkline[]> {
    const data = await super.getAll(config.nomicsApi + "/currencies/sparkline", {
      params: {
        ids: currency,
        key: process.env.VUE_APP_NOMICS_API_KEY,
        start: start.toISOString(),
        end: end.toISOString(),
        convert: store.getters.defaultCurrency,
        "per-page": 100,
      },
    });

    if (!data) {
      return Promise.reject();
    }

    return data.map((d) => Sparkline.map(d));
  }
}
